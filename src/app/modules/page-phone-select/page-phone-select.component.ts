import { Component, OnInit } from '@angular/core';
import {GetOutService} from '../../shared/services/get-out.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { parsePhoneNumberFromString } from 'libphonenumber-js/mobile';
import {CountryCode} from 'libphonenumber-js/types';

interface ISelectOption {
  name: string;
  callingCodes: string[];
  alpha2Code: CountryCode;
  number?: string;
}

@Component({
  selector: 'app-page-select',
  templateUrl: './page-phone-select.component.html',
  styleUrls: ['./page-phone-select.component.scss']
})
export class PagePhoneSelectComponent implements OnInit {

  public options: ISelectOption[] = [];
  public phonesForm: FormGroup;
  public valid = false;

  constructor(private getOut: GetOutService, private fb: FormBuilder) {
    this.phonesForm = fb.group({
      phone: ['', [Validators.required]]
    });
    this.phonesForm.valueChanges.subscribe((data) => {
      this.isValidPhone(data.phone);
    });
  }

  ngOnInit() {
    this.getOut.GetListDial().subscribe((data: ISelectOption[]) => {
      this.options = data;
    });
  }

  public isValidPhone(data: ISelectOption) {
    if (data.number) {
      const phoneNumber = parsePhoneNumberFromString(`+${data.callingCodes}${data.number}`, data.alpha2Code);
      if (phoneNumber) {
        this.valid = phoneNumber.isValid();
      }
    } else {
        this.valid = false;
    }
  }
}
