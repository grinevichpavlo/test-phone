import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PagePhoneSelectComponent} from './page-phone-select/page-phone-select.component';


const routes: Routes = [
  {
    path: 'phone-complete',
    component: PagePhoneSelectComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class ModulesRoutingModule {
}
