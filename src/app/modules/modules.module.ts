import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {ModulesRoutingModule} from './modules-routing.module';
import {PagePhoneSelectComponent} from './page-phone-select/page-phone-select.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    PagePhoneSelectComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    SharedModule,
    ModulesRoutingModule,
  ],
})
export class ModulesModule {
}
