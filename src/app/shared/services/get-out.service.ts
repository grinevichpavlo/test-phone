import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class GetOutService {
  private dialUrl = 'https://restcountries.eu/rest/v2/all?fields=name;alpha2Code;callingCodes';
  constructor(private http: HttpClient) {
  }

  public GetListDial() {
    return this.http.get(this.dialUrl);
  }
}
