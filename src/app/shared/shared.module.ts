import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneSelectComponent } from './phone-select/phone-select.component';
import { FlagComponent } from './flag/flag.component';
import {NumberDirective} from './numbers-only.directive';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    PhoneSelectComponent,
    FlagComponent,
    NumberDirective
  ],
  exports: [
    PhoneSelectComponent,
    FlagComponent,
  ]
})
export class SharedModule {
}
