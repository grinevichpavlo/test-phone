import {Component, ElementRef, forwardRef, Input, ViewChild} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

interface ISelectOption {
  name: string;
  callingCodes: string[];
  alpha2Code: string;
  number?: string;
}

@Component({
  selector: 'app-phone-select',
  template: `
    <div class="phone-select-wrapper">
      <div class="phone-select-form">
        <!-- Prefix -->
        <button (click)="toggleOpen()" type="button" class="prefix">+{{selectedDial?.callingCodes}}</button>
        <input [attr.disabled]="selectedDial?.callingCodes ? null : ''" #phone
               (keyup)="numberTrack.next($event)"
               class="form-control" numbersOnly>
        <!-- Suffix -->
      </div>
      <div [ngClass]="{'show': isOpen()}" class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <ul>
          <li *ngFor="let option of options"
              class="dropdown-item"
              [ngClass]="{'active':option.callingCodes === selectedDial?.callingCodes}"
              (click)="optionSelect(option.callingCodes);">
              <app-flag [code]="option.alpha2Code"></app-flag>
            {{ option.name }}
          </li>
        </ul>
        <div class="dropdown-item" *ngIf="!options.length">No items for select</div>
      </div>
    </div>
  `,
  styleUrls: ['./phone-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PhoneSelectComponent),
      multi: true,
    }
  ],
})

export class PhoneSelectComponent implements ControlValueAccessor {

  public numberTrack = new Subject<any>();
  public selectedDial: ISelectOption;
  public open = false;

  @ViewChild('phone') phoneRef: ElementRef;

  @Input() options: ISelectOption[] = [];

  constructor() {
    this.numberTrack.pipe(
      debounceTime(500),
      distinctUntilChanged()).subscribe(
      (event) => this.inputWrite(event.target.value));
  }

  public optionSelect(option) {
    this.writeValue({
        value: option.toString(),
        type: 'select',
    });
    this.onTouched();
    this.open = false;
  }

  public inputWrite(event) {
    this.writeValue({
        value: event,
        type: 'input',
    });
  }

  public isOpen() {
    return this.open;
  }

  public onTouched: any = () => {};

  public onChange: any = () => {};

  public toggleOpen() {
    this.open = !this.open;
  }

  registerOnChange(fn: (data) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

    writeValue(data) {
        if (!data.value || typeof data.value !== 'string') {
            return;
        }
        if (data.type === 'select') {
            const selectedEl = this.options.find(el => el.callingCodes.toString() === data.value);
            if (selectedEl) {
                this.onTouched(selectedEl);
                this.selectedDial = selectedEl;
                this.phoneRef.nativeElement.value = '';
                this.onChange(this.selectedDial);
            }
        } else {
          console.log(data.value);
            this.selectedDial.number = data.value;
            this.onChange(this.selectedDial);
        }
    }
}
